package com.ebulletin.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan("com.ebulletin")
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("com.ebulletin.repository")
@EnableJpaAuditing
@Import({SecurityContext.class})
public class ApplicationConfiguration {

}
