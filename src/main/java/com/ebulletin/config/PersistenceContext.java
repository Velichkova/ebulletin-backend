//package com.ebulletin.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.domain.AuditorAware;
//import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
//
//import com.ebulletin.utils.UsernameAuditorAware;
//
//@Configuration
//public class PersistenceContext {
//
//  @Bean
//  AuditorAware<String> auditorProvider() {
//    return new UsernameAuditorAware();
//  }
//
//}
