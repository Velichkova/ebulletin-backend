package com.ebulletin.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "article")
public class Article extends Auditable implements Serializable {

	@Id
	@Column(name = "id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long internalId;

	@Column
	@Type(type = "text")
	private String articleFile;
	
	@Column
	@Type(type = "text")
	private String title;
	
	@Column
	@Type(type = "text")
	private String authors;
	
	@Column
	@Type(type = "text")
	private String abstractText;
	
	@Column
	private String feeImage;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "bulletin_id", referencedColumnName = "id")
	private Bulletin bulletin;

	public Article() {
	}

	public Article(ArticleBuilder builder) {
		super(builder);
		this.articleFile = builder.articleFile;
		this.internalId = builder.internalId;
		this.user = builder.user;
		this.bulletin = builder.bulletin;
		this.title = builder.title;
		this.authors = builder.authors;
		this.abstractText = builder.abstractText;
		this.feeImage = builder.feeImage;
		this.createdDate = builder.createdDate;
		this.lastModifiedDate = builder.lastModifiedDate;
	}

	public Long getInternalId() {
		return internalId;
	}

	public void setInternalId(Long internalId) {
		this.internalId = internalId;
	}

	public String getArticleFile() {
		return articleFile;
	}

	public void setArticleFile(String articleFile) {
		this.articleFile = articleFile;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Bulletin getBulletin() {
		return bulletin;
	}

	public void setBulletin(Bulletin bulletin) {
		this.bulletin = bulletin;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public String getAbstractText() {
		return abstractText;
	}

	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}

	public String getFeeImage() {
		return feeImage;
	}

	public void setFeeImage(String feeImage) {
		this.feeImage = feeImage;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	public static class ArticleBuilder extends AuditableBuilder<ArticleBuilder> {
		private String articleFile;
		private Long internalId;
		private User user;
		private Bulletin bulletin;
		private String title;
		private String authors;
		private String abstractText;
		private LocalDateTime createdDate;
		private LocalDateTime lastModifiedDate;
		private String feeImage;

		public ArticleBuilder articleFile(String articleFile) {
			this.articleFile = articleFile;
			return this;
		}

		public ArticleBuilder internalId(Long internalId) {
			this.internalId = internalId;
			return this;
		}

		public ArticleBuilder user(User user) {
			this.user = user;
			return this;
		}
		
		public ArticleBuilder bulletin(Bulletin bulletin) {
			this.bulletin = bulletin;
			return this;
		}
		
		public ArticleBuilder title(String title) {
			this.title = title;
			return this;
		}
		
		public ArticleBuilder authors(String authors) {
			this.authors = authors;
			return this;
		}
		
		public ArticleBuilder abstractText(String abstractText) {
			this.abstractText = abstractText;
			return this;
		}
		
		public ArticleBuilder createdDate(LocalDateTime createdDate) {
			this.createdDate = createdDate;
			return this;
		}
		
		public ArticleBuilder lastModifiedDate(LocalDateTime lastModifiedDate) {
			this.lastModifiedDate = lastModifiedDate;
			return this;
		}
		
		public ArticleBuilder feeImage(String feeImage) {
			this.feeImage = feeImage;
			return this;
		}
		
		public Article build() {
			return new Article(this);
		}
	}

}
