package com.ebulletin.domain;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable {

	@Column(name = "uuid")
	@Type(type = "uuid-char")
	protected UUID id;

	@CreatedDate
	@Column(name = "creation_date", nullable = false, updatable = false)
	@Type(type = "org.jadira.usertype.dateandtime.threeten.PersistentLocalDateTime")
	protected LocalDateTime createdDate;

	@LastModifiedDate
	@Column(name = "modification_date")
	@Type(type = "org.jadira.usertype.dateandtime.threeten.PersistentLocalDateTime")
	protected LocalDateTime lastModifiedDate;

	protected Auditable() {
	}

	protected Auditable(AuditableBuilder<? extends AuditableBuilder> builder) {
		id = builder.id;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public UUID getUuid() {
		return id;
	}

	public String getId() {
		if (id == null) {
			return null;
		}

		return id.toString();
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public void setId(String id) {
		this.id = UUID.fromString(id);
	}

	public static class AuditableBuilder<T extends AuditableBuilder> {
		private UUID id;

		public AuditableBuilder() {
		}

		public T id(UUID id) {
			this.id = id;
			return (T) this;
		}

		public T id(String id) {
			if (StringUtils.isNotBlank(id)) {
				this.id = UUID.fromString(id);
			}
			return (T) this;
		}
	}

}
