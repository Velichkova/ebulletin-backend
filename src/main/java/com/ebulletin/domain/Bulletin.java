package com.ebulletin.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "bulletin")
public class Bulletin extends Auditable implements Serializable{
	
	@Id
	@Column(name = "id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long internalId;
	
	@Column(nullable = false)
	private Date startDate;
	
	@Column(nullable = false)
	private Date endDate;
	
	@Column
	@Type(type = "text")
	private String bulletinFile;
	
	@Column
	@Type(type = "text")
	private String issn;
	
	@Column
	private Integer vol;
	
	@Column
	@Type(type = "text")
	private String editionYear;
	
	public Bulletin() {}
	
	public Bulletin(BulletinBuilder builder) {
		super(builder);
		this.startDate = builder.startDate;
		this.endDate = builder.endDate;
		this.internalId = builder.internalId;
		this.bulletinFile = builder.bulletinFile;
		this.issn = builder.issn;
		this.vol = builder.vol;
		this.editionYear = builder.editionYear;
	}

	public Long getInternalId() {
		return internalId;
	}

	public void setInternalId(Long internalId) {
		this.internalId = internalId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public String getBulletinFile() {
		return bulletinFile;
	}

	public void setBulletinFile(String bulletinFile) {
		this.bulletinFile = bulletinFile;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public Integer getVol() {
		return vol;
	}

	public void setVol(Integer vol) {
		this.vol = vol;
	}

	public String getEditionYear() {
		return editionYear;
	}

	public void setEditionYear(String editionYear) {
		this.editionYear = editionYear;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	public static class BulletinBuilder extends AuditableBuilder<BulletinBuilder> {
		private Date startDate;
		private Date endDate;
		private Long internalId;
		private String bulletinFile;
		private String issn;
		private Integer vol;
		private String editionYear;
		
		public BulletinBuilder internalId(Long internalId) {
			this.internalId = internalId;
			return this;
		}
		
		public BulletinBuilder startDate(Date startDate) {
			this.startDate = startDate;
			return this;
		}
		
		public BulletinBuilder endDate(Date endDate) {
			this.endDate = endDate;
			return this;
		}
		
		public BulletinBuilder bulletinFile(String bulletinFile) {
			this.bulletinFile = bulletinFile;
			return this;
		}
		
		public BulletinBuilder issn(String issn) {
			this.issn = issn;
			return this;
		}
		
		public BulletinBuilder vol(Integer vol) {
			this.vol = vol;
			return this;
		}
		
		public BulletinBuilder editionYear(String editionYear) {
			this.editionYear = editionYear;
			return this;
		}
		
		public Bulletin build() {
			return new Bulletin(this);
		}
	}

}
