package com.ebulletin.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Type;

import com.ebulletin.domain.User.UserBuilder;

@Entity
@Table(name = "review")
public class Review extends Auditable implements Serializable {

	@Id
	@Column(name = "id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long internalId;

	@Column
	@Type(type = "text")
	private String reviewFile;

	@Column
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean accepted;
	
	@ManyToOne
	@JoinColumn(name = "article_id", referencedColumnName = "id")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name = "editor_id", referencedColumnName = "id")
	private User user;
	
	public Review() {}
	
	public Review(ArticleReviewBuilder builder) {
		super(builder);
		this.internalId = builder.internalId;
		this.reviewFile = builder.reviewFile;
		this.accepted = builder.accepted;
		this.article = builder.article;
		this.user = builder.user;
		this.createdDate = builder.createdDate;
	}

	public Long getInternalId() {
		return internalId;
	}

	public void setInternalId(Long internalId) {
		this.internalId = internalId;
	}

	public Boolean getAccepted() {
		return accepted;
	}

	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}
	
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getReviewFile() {
		return reviewFile;
	}

	public void setReviewFile(String reviewFile) {
		this.reviewFile = reviewFile;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
	public static class ArticleReviewBuilder extends AuditableBuilder<ArticleReviewBuilder> {
		private Long internalId;
		private String reviewFile;
		private Boolean accepted;
		private Article article;
		private User user;
		private LocalDateTime createdDate;
		
		public ArticleReviewBuilder internalId(Long internalId) {
			this.internalId = internalId;
			return this;
		}
		
		public ArticleReviewBuilder reviewFile(String reviewFile) {
			this.reviewFile = reviewFile;
			return this;
		}
		
		public ArticleReviewBuilder accepted(Boolean accepted) {
			this.accepted = accepted;
			return this;
		}
		
		public ArticleReviewBuilder article(Article article) {
			this.article = article;
			return this;
		}
		
		public ArticleReviewBuilder user(User user) {
			this.user = user;
			return this;
		}
		
		public ArticleReviewBuilder createdDate(LocalDateTime createdDate) {
			this.createdDate = createdDate;
			return this;
		}
		
		public Review build() {
			return new Review(this);
		}
	}
}
