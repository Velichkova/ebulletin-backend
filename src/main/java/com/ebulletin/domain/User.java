package com.ebulletin.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "user")
public class User extends Auditable implements Serializable {

	@Column(name = "uuid")
	@Type(type = "uuid-char")
	protected UUID id;

	@Id
	@Column(name = "id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long internalId;

	@Column(nullable = false)
	private String username;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String email;

	@Column(nullable = false)
	private String phone;

	@Column(nullable = false)
	private String faculty;

	@ManyToOne
	@JoinColumn(name = "user_role_id")
	private UserRole role;

	@Column(name = "forgotten_password_token")
	private String forgottenPasswordToken;
	
	@Column
	private String	articleIdConflict;

	public User() {

	}

	public User(UserBuilder builder) {
		this.username = builder.username;
		this.password = builder.password;
		this.name = builder.name;
		this.email = builder.email;
		this.phone = builder.phone;
		this.faculty = builder.faculty;
		this.role = builder.role;
		this.internalId = builder.internalId;
		this.id = builder.id;
		this.forgottenPasswordToken = builder.forgottenPasswordToken;
		this.articleIdConflict = builder.articleIdConflict;
		this.createdDate = builder.createdDate;
		this.lastModifiedDate = builder.lastModifiedDate;
	}

	public UUID getUuid() {
		return id;
	}

	public String getId() {
		if (id == null) {
			return null;
		}

		return id.toString();
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public void setId(String id) {
		this.id = UUID.fromString(id);
	}

	public Long getInternalId() {
		return internalId;
	}

	public void setInternalId(Long internalId) {
		this.internalId = internalId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public String getForgottenPasswordToken() {
		return forgottenPasswordToken;
	}

	public void setForgottenPasswordToken(String forgottenPasswordToken) {
		this.forgottenPasswordToken = forgottenPasswordToken;
	}

	public String getArticleIdConflict() {
		return articleIdConflict;
	}

	public void setArticleIdConflict(String articleIdConflict) {
		this.articleIdConflict = articleIdConflict;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	public static class UserBuilder {
		private String username;
		private String password;
		private String name;
		private String email;
		private String phone;
		private String faculty;
		private UserRole role;
		private UUID id;
		private Long internalId;
		private String forgottenPasswordToken;
		private String articleIdConflict;
		private LocalDateTime createdDate;
		private LocalDateTime lastModifiedDate;
		
		public UserBuilder username(String username) {
			this.username = username;
			return this;
		}

		public UserBuilder password(String password) {
			this.password = password;
			return this;
		}

		public UserBuilder name(String name) {
			this.name = name;
			return this;
		}

		public UserBuilder email(String email) {
			this.email = email;
			return this;
		}

		public UserBuilder phone(String phone) {
			this.phone = phone;
			return this;
		}

		public UserBuilder faculty(String faculty) {
			this.faculty = faculty;
			return this;
		}

		public UserBuilder role(UserRole role) {
			this.role = role;
			return this;
		}

		public UserBuilder id(UUID id) {
			this.id = id;
			return this;
		}

		public UserBuilder id(String id) {
			if (StringUtils.isNotBlank(id)) {
				this.id = UUID.fromString(id);
			}
			return this;
		}

		public UserBuilder internalId(Long internalId) {
			this.internalId = internalId;
			return this;
		}

		public UserBuilder forgottenPasswordToken(String forgottenPasswordToken) {
			this.forgottenPasswordToken = forgottenPasswordToken;
			return this;
		}

		public UserBuilder articleIdConflict(String articleIdConflict) {
			this.articleIdConflict = articleIdConflict;
			return this;
		}
		
		public UserBuilder createdDate(LocalDateTime createdDate) {
			this.createdDate = createdDate;
			return this;
		}
		
		public UserBuilder lastModifiedDate(LocalDateTime lastModifiedDate) {
			this.lastModifiedDate = lastModifiedDate;
			return this;
		}
		
		public User build() {
			return new User(this);
		}
	}
}
