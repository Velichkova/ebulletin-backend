package com.ebulletin.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
@Table(name = "user_role")
public class UserRole implements Serializable {

	@Id
	@Column(name = "id", updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String description;

	public UserRole() {
	};

	public UserRole(UserRoleBuilder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.description = builder.description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	//kude go polzvam???
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	public static class UserRoleBuilder {
		private Long id;
		private String name;
		private String description;

		public UserRoleBuilder id(Long id) {
			this.id = id;
			return this;
		}

		public UserRoleBuilder name(String name) {
			this.name = name;
			return this;
		}

		public UserRoleBuilder description(String description) {
			this.description = description;
			return this;
		}

		public UserRole build() {
			return new UserRole(this);
		}
	}
}
