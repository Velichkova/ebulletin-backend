package com.ebulletin.exception;

public class StorageFileNotFoundException extends StorageException {

	  private static final long serialVersionUID = 4128282809109493099L;

	  public StorageFileNotFoundException(String message) {
	    super(message);
	  }

	  public StorageFileNotFoundException(String message, Throwable cause) {
	    super(message, cause);
	  }
	}
