package com.ebulletin.repository;

import org.springframework.data.repository.CrudRepository;

import com.ebulletin.domain.ArticleArchive;

public interface ArticleArchiveRepository extends CrudRepository<ArticleArchive, Long> {

}
