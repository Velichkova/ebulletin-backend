package com.ebulletin.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ebulletin.domain.Article;

public interface ArticleRepository extends CrudRepository<Article, Long> {

	@Query(value = "select e.internalId from Article e where e.id = :id")
	Long getInternalId(@Param("id") UUID id);

	@Query(value = "select e from Article e where e.user.id = :id")
	Iterable<Article> findByUser(@Param("id") UUID id);
	
	@Query(value = "select e from Article e where e.bulletin.id = :id")
	Iterable<Article> getArticles(@Param("id") UUID id);
}
