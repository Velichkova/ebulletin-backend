package com.ebulletin.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ebulletin.domain.Bulletin;

public interface BulletinRepository extends CrudRepository<Bulletin, Long> {

	@Query(value = "select e.internalId from Bulletin e where e.id = :id")
	Long getInternalId(@Param("id") UUID id);

	@Query(value = "select e from Bulletin e where e.id = :id")
	Bulletin findById(@Param("id") UUID id);
}
