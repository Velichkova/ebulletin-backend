package com.ebulletin.repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ebulletin.domain.Review;

public interface ReviewRepository extends CrudRepository<Review, Long> {

	@Query(value = "select e.internalId from Review e where e.id = :id")
	Long getInternalId(@Param("id") UUID id);

	@Query(value = "select e from Review e where e.article.id = :id")
	List<Review> getByArticleId(@Param("id") UUID id);

	@Query(value = "select cc.internalId from Review cc where cc.id in (:ids)")
	List<Long> getInternalIds(@Param("ids") Set<UUID> ids);
	
}
