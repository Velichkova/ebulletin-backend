package com.ebulletin.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ebulletin.domain.User;

public interface UserRepository extends CrudRepository<User, Long> {

	@Query(value = "select e from User e where e.username = :username and e.password = :password")
	User authenticate(@Param("username") String username, @Param("password") String password);

	@Query(value = "select e.internalId from User e where e.id = :id")
	Long getInternalId(@Param("id") UUID id);

	@Query(value = "select e from User e where e.id = :id")
	User findById(@Param("id") UUID id);

	@Query(value = "select e from User e where e.username = :username")
	User findByUsername(@Param("username") String username);
	
	@Query(value = "select e from User e where e.email = :email")
	User findByEmail(@Param("email") String email);
	
	User getByForgottenPasswordToken(@Param("token") String token);
}
