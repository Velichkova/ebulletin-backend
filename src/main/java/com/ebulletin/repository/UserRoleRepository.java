package com.ebulletin.repository;

import org.springframework.data.repository.CrudRepository;

import com.ebulletin.domain.UserRole;

public interface UserRoleRepository extends CrudRepository<UserRole, Long>{

}
