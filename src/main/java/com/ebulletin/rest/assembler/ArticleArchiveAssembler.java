package com.ebulletin.rest.assembler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ebulletin.domain.Article;
import com.ebulletin.domain.ArticleArchive;
import com.ebulletin.rest.resource.ArticleArchiveResource;

@Component
public class ArticleArchiveAssembler {

	@Autowired
	private UserAssembler userAssembler;

	@Autowired
	private BulletinAssembler bulletinAssembler;
	
	public ArticleArchive fromArticle(Article article) {
		if (article == null) {
			return null;
		}

		ArticleArchive resource = new ArticleArchive();
		resource.setId(article.getId());
		resource.setArticleFile(article.getArticleFile());
		resource.setUser(article.getUser());
		resource.setBulletin(article.getBulletin());
		resource.setTitle(article.getTitle());
		resource.setAuthors(article.getAuthors());
		resource.setAbstractText(article.getAbstractText());
		resource.setCreatedDate(article.getCreatedDate());
		resource.setLastModifiedDate(article.getLastModifiedDate());
		resource.setFeeImage(article.getFeeImage());

		return resource;
	}
	
	public List<ArticleArchiveResource> fromArticles(Iterable<ArticleArchive> articles) {
		if (articles == null) {
			return null;
		}

		List<ArticleArchiveResource> resources = new ArrayList<>();

		for (ArticleArchive article : articles) {
			resources.add(fromArticleToResource(article));
		}
		return resources;
	}

	public ArticleArchiveResource fromArticleToResource(ArticleArchive article) {
		if (article == null) {
			return null;
		}

		ArticleArchiveResource resource = new ArticleArchiveResource();
		resource.setId(article.getId());
		resource.setArticleFile(article.getArticleFile());
		resource.setUser(userAssembler.fromUser(article.getUser()));
		resource.setBulletin(bulletinAssembler.fromBulletin(article.getBulletin()));
		resource.setTitle(article.getTitle());
		resource.setAuthors(article.getAuthors());
		resource.setAbstractText(article.getAbstractText());
		resource.setCreatedDate(article.getCreatedDate());
		resource.setLastModifiedDate(article.getLastModifiedDate());
		resource.setFeeImage(article.getFeeImage());

		return resource;
	}
}
