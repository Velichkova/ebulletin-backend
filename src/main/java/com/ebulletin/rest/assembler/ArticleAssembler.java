package com.ebulletin.rest.assembler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ebulletin.domain.Article;
import com.ebulletin.domain.Article.ArticleBuilder;
import com.ebulletin.rest.resource.ArticleResource;

@Component
public class ArticleAssembler {

	@Autowired
	private UserAssembler userAssembler;

	@Autowired
	private BulletinAssembler bulletinAssembler;

	public List<ArticleResource> fromArticles(Iterable<Article> articles) {
		if (articles == null) {
			return null;
		}

		List<ArticleResource> resources = new ArrayList<>();

		for (Article article : articles) {
			resources.add(fromArticle(article));
		}
		return resources;
	}

	public ArticleResource fromArticle(Article article) {
		if (article == null) {
			return null;
		}

		ArticleResource resource = new ArticleResource();
		resource.setId(article.getId());
		resource.setArticleFile(article.getArticleFile());
		resource.setUser(userAssembler.fromUser(article.getUser()));
		resource.setBulletin(bulletinAssembler.fromBulletin(article.getBulletin()));
		resource.setTitle(article.getTitle());
		resource.setAuthors(article.getAuthors());
		resource.setAbstractText(article.getAbstractText());
		resource.setCreatedDate(article.getCreatedDate());
		resource.setLastModifiedDate(article.getLastModifiedDate());
		resource.setFeeImage(article.getFeeImage());

		return resource;
	}

	public Article toArticle(ArticleResource resource) {
		if (resource == null) {
			return null;
		}

		ArticleBuilder builder = new ArticleBuilder();
		builder
		.id(resource.getId())
		.articleFile(resource.getArticleFile())
		.user(userAssembler.toUser(resource.getUser()))
		.bulletin(bulletinAssembler.toBulletin(resource.getBulletin()))
		.title(resource.getTitle())
		.authors(resource.getAuthors())
		.abstractText(resource.getAbstractText())
		.createdDate(resource.getCreatedDate())
		.lastModifiedDate(resource.getLastModifiedDate())
		.feeImage(resource.getFeeImage());

		return builder.build();
	}
}
