package com.ebulletin.rest.assembler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.ebulletin.domain.Bulletin;
import com.ebulletin.domain.Bulletin.BulletinBuilder;
import com.ebulletin.rest.resource.BulletinResource;

@Component
public class BulletinAssembler {

	public List<BulletinResource> fromBulletins(Iterable<Bulletin> bulletins) {
		if (bulletins == null) {
			return null;
		}

		List<BulletinResource> resources = new ArrayList<>();

		for (Bulletin bulletin : bulletins) {
			resources.add(fromBulletin(bulletin));
		}
		return resources;
	}

	public BulletinResource fromBulletin(Bulletin bulletin) {
		if (bulletin == null) {
			return null;
		}

		BulletinResource resource = new BulletinResource();
		resource.setId(bulletin.getId());
		resource.setEndDate(bulletin.getEndDate());
		resource.setStartDate(bulletin.getStartDate());
		resource.setBulletinFile(bulletin.getBulletinFile());
		resource.setIssn(bulletin.getIssn());
		resource.setVol(bulletin.getVol());
		resource.setEditionYear(bulletin.getEditionYear());

		return resource;
	}

	public Bulletin toBulletin(BulletinResource resource) {
		if (resource == null) {
			return null;
		}

		BulletinBuilder builder = new BulletinBuilder();
		builder.id(resource.getId()).startDate(resource.getStartDate()).endDate(resource.getEndDate())
		.bulletinFile(resource.getBulletinFile()).issn(resource.getIssn()).vol(resource.getVol())
		.editionYear(resource.getEditionYear());
		
		return builder.build();
	}
}
