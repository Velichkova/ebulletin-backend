package com.ebulletin.rest.assembler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ebulletin.domain.Review;
import com.ebulletin.domain.Review.ArticleReviewBuilder;
import com.ebulletin.rest.resource.ReviewResource;

@Component
public class ReviewAssembler {
	
	@Autowired
	private ArticleAssembler articleAssembler;
	
	@Autowired
	private UserAssembler userAssembler;
	
	public List<ReviewResource> fromReviews(Iterable<Review> reviews) {
		if (reviews == null) {
			return null;
		}

		List<ReviewResource> resources = new ArrayList<>();

		for (Review review : reviews) {
			resources.add(fromReview(review));
		}
		return resources;
	}
	
	public ReviewResource fromReview(Review review) {
		if (review == null) {
			return null;
		}

		ReviewResource resource = new ReviewResource();
		resource.setId(review.getId());
		resource.setReviewFile(review.getReviewFile());
		resource.setAccepted(review.getAccepted());
		resource.setArticle(articleAssembler.fromArticle(review.getArticle()));
		resource.setUser(userAssembler.fromUser(review.getUser()));
		resource.setCreatedDate(review.getCreatedDate());

		return resource;
	}

	public Review toReview(ReviewResource resource) {
		if (resource == null) {
			return null;
		}

		ArticleReviewBuilder builder = new ArticleReviewBuilder();
		builder
		.id(resource.getId())
		.reviewFile(resource.getReviewFile())
		.accepted(resource.getAccepted())
		.article(articleAssembler.toArticle(resource.getArticle()))
		.user(userAssembler.toUser(resource.getUser()))
		.createdDate(resource.getCreatedDate());

		return builder.build();
	}
	
//	public Set<Review> toReviews(Set<ReviewResource> resources){
//		if (resources == null) {
//			return new HashSet<Review>();
//		}
//		
//		Set<Review> reviews = new HashSet<>();
//
//	    for (ReviewResource resource : resources) {
//	    	reviews.add(toReview(resource));
//	    }
//
//	    return reviews;
//	}
}
