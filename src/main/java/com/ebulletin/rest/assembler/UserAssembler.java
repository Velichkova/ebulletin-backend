package com.ebulletin.rest.assembler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ebulletin.domain.User;
import com.ebulletin.domain.User.UserBuilder;
import com.ebulletin.rest.resource.UserResource;

@Component
public class UserAssembler {

	@Autowired
	private UserRoleAssembler userRoleAssembler;

	public List<UserResource> fromUsers(Iterable<User> users) {
		if (users == null) {
			return null;
		}

		List<UserResource> resources = new ArrayList<>();

		for (User user : users) {
			resources.add(fromUser(user));
		}

		return resources;
	}

	public UserResource fromUser(User user) {
		if (user == null) {
			return null;
		}

		UserResource resource = new UserResource();
		resource.setId(user.getId());
		resource.setUsername(user.getUsername());
		resource.setPassword(resource.getPassword());
		resource.setName(user.getName());
		resource.setEmail(user.getEmail());
		resource.setPhone(user.getPhone());
		resource.setFaculty(user.getFaculty());
		resource.setRole(userRoleAssembler.fromUserRole(user.getRole()));
		resource.setArticleIdConflict(user.getArticleIdConflict());
		resource.setCreatedDate(user.getCreatedDate());
		resource.setLastModifiedDate(user.getLastModifiedDate());

		return resource;
	}

	public User toUser(UserResource resource) {
		if (resource == null) {
			return null;
		}

		UserBuilder builder = new UserBuilder();
		builder
		.id(resource.getId())
		.username(resource.getUsername())
		.password(resource.getPassword())
		.name(resource.getName())
		.email(resource.getEmail())
		.phone(resource.getPhone())
		.faculty(resource.getFaculty())
		.role(userRoleAssembler.toUserRole(resource.getRole()))
		.createdDate(resource.getCreatedDate())
		.lastModifiedDate(resource.getLastModifiedDate())
		.articleIdConflict(resource.getArticleIdConflict());

		return builder.build();
	}
}
