package com.ebulletin.rest.assembler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.ebulletin.domain.UserRole;
import com.ebulletin.domain.UserRole.UserRoleBuilder;
import com.ebulletin.rest.resource.UserRoleResource;

@Component
public class UserRoleAssembler {

	public List<UserRoleResource> fromUserRoles(Iterable<UserRole> userRoles) {
		List<UserRoleResource> userRoleResources = new ArrayList<>();

		for (UserRole userRole : userRoles) {
			userRoleResources.add(fromUserRole(userRole));
		}

		return userRoleResources;
	}

	public UserRoleResource fromUserRole(UserRole userRole) {
		if (userRole == null) {
			return null;
		}

		UserRoleResource resource = new UserRoleResource();
		resource.setId(userRole.getId());
		resource.setName(userRole.getName());
		resource.setDescription(userRole.getDescription());

		return resource;
	}

	public UserRole toUserRole(UserRoleResource resource) {
		if (resource == null) {
			return null;
		}

		UserRoleBuilder builder = new UserRoleBuilder();
		builder.id(resource.getId()).name(resource.getName()).description(resource.getDescription());

		return builder.build();
	}
}
