package com.ebulletin.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ebulletin.domain.ArticleArchive;
import com.ebulletin.rest.assembler.ArticleArchiveAssembler;
import com.ebulletin.rest.resource.ArticleArchiveResource;
import com.ebulletin.service.ArticleArchiveService;

@RestController
@RequestMapping("v1/articleArchive")
@CrossOrigin(origins = "http://localhost:4200")
public class ArticleArchiveController {

	@Autowired
	ArticleArchiveService articleArchiveService;

	@Autowired
	ArticleArchiveAssembler articleArchiveAssembler;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ArticleArchiveResource>> getAll() {

		Iterable<ArticleArchive> articles = articleArchiveService.getAll();

		List<ArticleArchiveResource> articleResource = articleArchiveAssembler.fromArticles(articles);

		return new ResponseEntity<>(articleResource, HttpStatus.OK);
	}

}
