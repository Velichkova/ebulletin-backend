package com.ebulletin.rest.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.tika.exception.TikaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import com.ebulletin.domain.Article;
import com.ebulletin.rest.assembler.ArticleAssembler;
import com.ebulletin.rest.resource.ArticleResource;
import com.ebulletin.service.ArticleService;

@RestController
@RequestMapping("v1/article")
@CrossOrigin(origins = "http://localhost:4200")
public class ArticleController {

	@Autowired
	ArticleService articleService;

	@Autowired
	ArticleAssembler articleAssembler;

	@PreAuthorize("hasAnyRole('ROLE_Admin', 'ROLE_editor')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ArticleResource>> getAll() {

		Iterable<Article> articles = articleService.getAll();

		List<ArticleResource> articleResource = articleAssembler.fromArticles(articles);

		return new ResponseEntity<>(articleResource, HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_user')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<ArticleResource> create(@RequestBody ArticleResource articleResource) throws Exception {
		Article article = articleAssembler.toArticle(articleResource);
		Article responseArticle = articleService.create(article);
		
		ArticleResource response = articleAssembler.fromArticle(responseArticle);

		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{articleId}")
	public ResponseEntity<ArticleResource> update(@PathVariable String articleId,
			@Valid @RequestBody ArticleResource articleResource) throws Exception {

		if (!articleId.equals(articleResource.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Article article = articleAssembler.toArticle(articleResource);
		article.setId(articleId);
		Article responseArticle = articleService.update(article);
		ArticleResource response = articleAssembler.fromArticle(responseArticle);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_Admin')")
	@RequestMapping(method = RequestMethod.DELETE, value = "/{articleId}")
	public ResponseEntity<ArticleResource> delete(@PathVariable String articleId) {
		articleService.delete(articleId);

		return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
	}

	@PreAuthorize("hasAnyRole('ROLE_user')")
	@RequestMapping(method = RequestMethod.GET, value = "/{userId}")
	public ResponseEntity<List<ArticleResource>> getForUser(@PathVariable String userId) {
		Iterable<Article> resourceArticles = articleService.getForUser(UUID.fromString(userId));

		List<ArticleResource> resource = articleAssembler.fromArticles(resourceArticles);

		return new ResponseEntity<>(resource, HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_user')")
	@RequestMapping(method = RequestMethod.POST, value = "/{articleId}/file", produces = "text/pdf")
	public ResponseEntity<?> uploadFile(@PathVariable String articleId, @RequestParam("file") MultipartFile file)
			throws UnsupportedEncodingException, IOException, SAXException, TikaException {
		articleService.saveFile(file, articleId);

		return ResponseEntity.ok().build();
	}

	@PreAuthorize("hasAnyRole('ROLE_user')")
	@RequestMapping(method = RequestMethod.POST, value = "/{articleId}/feeImage", produces = "image/jpeg")
	public ResponseEntity<?> uploadFeeImage(@PathVariable String articleId, @RequestParam("file") MultipartFile file)
			throws UnsupportedEncodingException, IOException, SAXException, TikaException {
		articleService.saveFeeImage(file, articleId);

		return ResponseEntity.ok().build();
	}

	@PreAuthorize("hasAnyRole('ROLE_Admin', 'ROLE_editor', 'ROLE_user')")
	@RequestMapping(method = RequestMethod.GET, value = "/{articleId}/file", produces = "application/json")
	public ResponseEntity<byte[]> getfile(@PathVariable String articleId) {
		return new ResponseEntity<byte[]>(articleService.getFile(articleId), HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_Admin', 'ROLE_editor', 'ROLE_user')")
	@RequestMapping(method = RequestMethod.GET, value = "/{articleId}/feeImage")
	public ResponseEntity<byte[]> getFeeImage(@PathVariable String articleId) {
		return new ResponseEntity<byte[]>(articleService.getFeeImage(articleId), HttpStatus.OK);
	}

}
