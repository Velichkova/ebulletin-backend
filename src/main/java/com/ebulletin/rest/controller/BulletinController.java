package com.ebulletin.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ebulletin.domain.Bulletin;
import com.ebulletin.rest.assembler.ArticleAssembler;
import com.ebulletin.rest.assembler.BulletinAssembler;
import com.ebulletin.rest.resource.BulletinResource;
import com.ebulletin.service.BulletinService;

@RestController
@RequestMapping("v1/bulletin")
@CrossOrigin(origins = "http://localhost:4200")
public class BulletinController {
	
	@Autowired
	BulletinService bulletinService;

	@Autowired
	BulletinAssembler bulletinAssembler;
	
	@Autowired
	ArticleAssembler articleAssembler;
	
	//@PreAuthorize("hasAnyRole('ROLE_Admin', 'ROLE_user', 'ROLE_editor')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<BulletinResource>> getAll() {

		Iterable<Bulletin> bulletins = bulletinService.getAll();

		List<BulletinResource> bulletinResource = bulletinAssembler.fromBulletins(bulletins);

		return new ResponseEntity<>(bulletinResource, HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_Admin')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<BulletinResource> create(@RequestBody BulletinResource bulletinResource) throws Exception {
		Bulletin bulletin = bulletinAssembler.toBulletin(bulletinResource);
		Bulletin responseBulletin = bulletinService.create(bulletin);
		BulletinResource response = bulletinAssembler.fromBulletin(responseBulletin);

		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@PreAuthorize("hasAnyRole('ROLE_Admin')")
	@RequestMapping(method = RequestMethod.PUT, value = "/{bulletinId}")
	public ResponseEntity<BulletinResource> update(@PathVariable String bulletinId,
			@Valid @RequestBody BulletinResource bulletinResource) throws Exception {

		if (!bulletinId.equals(bulletinResource.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Bulletin bulletin = bulletinAssembler.toBulletin(bulletinResource);
		bulletin.setId(bulletinId);
		Bulletin responseBulletin = bulletinService.update(bulletin);
		BulletinResource response = bulletinAssembler.fromBulletin(responseBulletin);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_Admin')")
	@RequestMapping(method = RequestMethod.DELETE, value = "/{bulletinId}")
	public ResponseEntity<BulletinResource> delete(@PathVariable String bulletinId) {
		bulletinService.delete(bulletinId);

		return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_Admin')")
	@RequestMapping(method = RequestMethod.GET, value = "/{bulletinId}/file", produces = "application/json")
	public ResponseEntity<byte[]> getfile(@PathVariable String bulletinId) throws IOException {
		bulletinService.generateFile(bulletinId);
		bulletinService.getFile(bulletinId);
		return new ResponseEntity<byte[]>(bulletinService.getFile(bulletinId), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{bulletinId}")
	public ResponseEntity<BulletinResource> getBulletin(@PathVariable String bulletinId) {
		Bulletin bulletinResource = bulletinService.getBulletin(UUID.fromString(bulletinId));

		BulletinResource resource = bulletinAssembler.fromBulletin(bulletinResource);

		return new ResponseEntity<>(resource, HttpStatus.OK);
	}

}
