package com.ebulletin.rest.controller;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ebulletin.rest.resource.EmailResource;
import com.ebulletin.utils.EmailUtils;

@RestController
@RequestMapping("v1/email")
@CrossOrigin("*")
public class EmailController {

	@PreAuthorize("hasAnyRole('ROLE_Admin')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<EmailResource> sendMail(@RequestBody EmailResource resource) throws Exception {

		EmailUtils.sendMail(resource.getFrom(), resource.getTo(), resource.getSubject(), resource.getContent());

		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/signup")
	public ResponseEntity<EmailResource> sendSignedUpMail(@RequestBody final EmailResource resource) throws Exception {

		new Thread(new Runnable() {
			public void run() {
				try {
					EmailUtils.sendSignedUp(resource.getTo());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();

		return new ResponseEntity<>(null, HttpStatus.OK);
	}
}