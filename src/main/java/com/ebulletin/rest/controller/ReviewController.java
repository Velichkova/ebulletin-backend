package com.ebulletin.rest.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.validation.Valid;

import org.apache.tika.exception.TikaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import com.ebulletin.domain.Review;
import com.ebulletin.rest.assembler.ReviewAssembler;
import com.ebulletin.rest.resource.ReviewResource;
import com.ebulletin.service.ReviewService;

@RestController
@RequestMapping("v1/review")
@CrossOrigin(origins = "http://localhost:4200")
public class ReviewController {
	
	@Autowired
	ReviewAssembler reviewAssembler;
	
	@Autowired
	ReviewService reviewService;
	
	@PreAuthorize("hasAnyRole('ROLE_Admin', 'ROLE_user', 'ROLE_editor')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ReviewResource>> getAll() {

		Iterable<Review> reviews = reviewService.getAll();

		List<ReviewResource> reviewResource = reviewAssembler.fromReviews(reviews);

		return new ResponseEntity<>(reviewResource, HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_editor')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<ReviewResource> create(@RequestBody ReviewResource reviewResource) throws Exception {
		Review review = reviewAssembler.toReview(reviewResource);
		Review responseReview = reviewService.create(review);
		ReviewResource response = reviewAssembler.fromReview(responseReview);

		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_Admin')")
	@RequestMapping(method = RequestMethod.DELETE, value = "/{reviewId}")
	public ResponseEntity<ReviewResource> delete(@PathVariable String reviewId) {
		reviewService.delete(reviewId);

		return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
	}

	@PreAuthorize("hasAnyRole('ROLE_Admin', 'ROLE_user', 'ROLE_editor')")
	@RequestMapping(method = RequestMethod.GET, value = "/{articleId}")
	public ResponseEntity<List<ReviewResource>> getAllForArticle(
			@PathVariable String articleId) {

		Iterable<Review> reviews = reviewService.getAllForArticle(articleId);

		List<ReviewResource> reviewResource = reviewAssembler.fromReviews(reviews);

		return new ResponseEntity<>(reviewResource, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_editor')")
	@RequestMapping(method = RequestMethod.POST, value = "/{reviewId}/file")
	public ResponseEntity<?> uploadFile(@PathVariable String reviewId, @RequestParam("file") MultipartFile file) throws UnsupportedEncodingException, IOException, SAXException, TikaException {

		reviewService.saveFile(file, reviewId);

		return ResponseEntity.ok().build();
	}
	
	@PreAuthorize("hasAnyRole('ROLE_editor')")
	@RequestMapping(method = RequestMethod.PUT, value = "/{reviewId}/accepted")
	public ResponseEntity<ReviewResource> saveDecision(@PathVariable String reviewId,
			@Valid @RequestBody ReviewResource reviewResource){

		if (!reviewId.equals(reviewResource.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Review review = reviewAssembler.toReview(reviewResource);
		review.setId(reviewId);
		Review responseReview = reviewService.update(review);
		ReviewResource response = reviewAssembler.fromReview(responseReview);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_user')")
	@RequestMapping(method = RequestMethod.GET, value = "/{articleId}/{reviewNeeded}", produces = "application/json")
	public ResponseEntity<byte[]> getfiles(@PathVariable String articleId,  
			@PathVariable int reviewNeeded) {
		//1 or 2 return 1 or 2 review depends on date created
		byte[] response = reviewService.getFiles(articleId, reviewNeeded);
		
		return new ResponseEntity<byte[]>(response, HttpStatus.OK);
	}

}
