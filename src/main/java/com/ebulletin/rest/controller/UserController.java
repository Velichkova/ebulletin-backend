package com.ebulletin.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ebulletin.domain.User;
import com.ebulletin.rest.assembler.UserAssembler;
import com.ebulletin.rest.resource.UserResource;
import com.ebulletin.service.UserService;
import com.ebulletin.utils.EmailUtils;

@RestController
@RequestMapping("v1/users")
@CrossOrigin("*")
public class UserController {

	@Autowired
	ServletContext servletContext;

	@Autowired
	UserService userService;

	@Autowired
	UserAssembler userAssembler;

	@PreAuthorize("hasAnyRole('ROLE_Admin')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<UserResource>> getAll() {

		Iterable<User> users = userService.getAll();

		List<UserResource> userResources = userAssembler.fromUsers(users);

		return new ResponseEntity<>(userResources, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public ResponseEntity<UserResource> create(@RequestBody UserResource userResource) throws Exception {
		User user = userAssembler.toUser(userResource);
		User responseUser = userService.create(user);
		UserResource response = userAssembler.fromUser(responseUser);

		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/authenticate")
	public ResponseEntity<UserResource> authenticate(@RequestBody UserResource userResource) throws Exception {
		User user = userAssembler.toUser(userResource);
		try {
			user = userService.authenticate(user);
			UserResource response = userAssembler.fromUser(user);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		} catch (SecurityException e) {
			return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
		}
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{userId}")
	public ResponseEntity<UserResource> update(@PathVariable String userId,
			@Valid @RequestBody UserResource userResource) throws Exception {

		if (!userId.equals(userResource.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		User user = userAssembler.toUser(userResource);
		user.setId(userId);
		User responseUser = userService.update(user, true);
		UserResource response = userAssembler.fromUser(responseUser);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_Admin')")
	@RequestMapping(method = RequestMethod.DELETE, value = "/{userId}")
	public ResponseEntity<UserResource> delete(@PathVariable String userId) {
		userService.delete(userId);

		return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/forgotten-password")
	public ResponseEntity<UserResource> findForgottenPasswordToken(@RequestParam(value = "email") String userEmail)
			throws Exception {

		if (userEmail.equals(null)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		final User user = userService.getByEmail(userEmail);

		String token = UUID.randomUUID().toString();
		user.setForgottenPasswordToken(token);

		try {
			userService.update(user, false);

			new Thread(new Runnable() {
				public void run() {
					try {
						EmailUtils.sendForgottenPassword(user.getEmail(), user.getForgottenPasswordToken());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}).start();

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/forgotten-password")
	public ResponseEntity<UserResource> getByForgottenPasswordToken(@RequestParam(value = "token") String token) {

		if (token.equals(null)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		User user = userService.getByForgottenPasswordToken(token);

		UserResource userResource = userAssembler.fromUser(user);
		userResource.setPassword("");

		return new ResponseEntity<>(userResource, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{email}")
	public ResponseEntity<UserResource> getUserByEmail(@PathVariable String email) {
		User user = userService.getByEmail(email);
		if (user == null) {
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
		UserResource response = userAssembler.fromUser(user);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
