package com.ebulletin.rest.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ebulletin.domain.UserRole;
import com.ebulletin.rest.assembler.UserRoleAssembler;
import com.ebulletin.rest.resource.UserRoleResource;
import com.ebulletin.service.UserRoleService;

@RestController
@RequestMapping("v1/user-roles")
public class UserRoleController {

	@Autowired
	UserRoleService userRoleService;

	@Autowired
	UserRoleAssembler userRoleAssembler;

	@PreAuthorize("hasAnyRole('ROLE_Admin')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<UserRoleResource>> getAll() {
		Iterable<UserRole> userRoles = userRoleService.getAll();

		List<UserRoleResource> userRoleResource = userRoleAssembler.fromUserRoles(userRoles);

		return new ResponseEntity<>(userRoleResource, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Iterable<UserRoleResource>> create() {
		Iterable<UserRole> userRoles;
		try {
			userRoles = userRoleService.create();
			Iterable<UserRoleResource> response = userRoleAssembler.fromUserRoles(userRoles);

			return new ResponseEntity<>(response, HttpStatus.CREATED);
		} catch (IOException e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
