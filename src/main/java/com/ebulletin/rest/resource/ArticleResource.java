package com.ebulletin.rest.resource;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class ArticleResource {

	private String id;
	private String articleFile;
	private UserResource user;
	private BulletinResource bulletin;
	private String title;
	private String authors;
	private String abstractText;
	private LocalDateTime createdDate;
	private LocalDateTime lastModifiedDate;
	private String feeImage;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getArticleFile() {
		return articleFile;
	}

	public void setArticleFile(String articleFile) {
		this.articleFile = articleFile;
	}

	public UserResource getUser() {
		return user;
	}

	public void setUser(UserResource user) {
		this.user = user;
	}

	public BulletinResource getBulletin() {
		return bulletin;
	}

	public void setBulletin(BulletinResource bulletin) {
		this.bulletin = bulletin;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public String getAbstractText() {
		return abstractText;
	}

	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getFeeImage() {
		return feeImage;
	}

	public void setFeeImage(String feeImage) {
		this.feeImage = feeImage;
	}

}
