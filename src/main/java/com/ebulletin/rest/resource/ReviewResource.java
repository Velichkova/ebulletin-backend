package com.ebulletin.rest.resource;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class ReviewResource {

	private String id;
	private String reviewFile;
	private Boolean accepted;
	private ArticleResource article;
	private UserResource user;
	private LocalDateTime createdDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReviewFile() {
		return reviewFile;
	}
	public void setReviewFile(String reviewFile) {
		this.reviewFile = reviewFile;
	}
	public Boolean getAccepted() {
		return accepted;
	}
	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}
	public ArticleResource getArticle() {
		return article;
	}
	public void setArticle(ArticleResource article) {
		this.article = article;
	}
	public UserResource getUser() {
		return user;
	}
	public void setUser(UserResource user) {
		this.user = user;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	
}
