package com.ebulletin.rest.resource;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class UserResource {
	private String id;
	private String username;
	private String password;
	private String name;
	private String email;
	private String phone;
	private String faculty;
	private UserRoleResource role;
	private String forgottenPasswordToken;
	private String articleIdConflict;
	private LocalDateTime createdDate;
	private LocalDateTime lastModifiedDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFaculty() {
		return faculty;
	}
	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}
	public UserRoleResource getRole() {
		return role;
	}
	public void setRole(UserRoleResource role) {
		this.role = role;
	}
	public String getForgottenPasswordToken() {
		return forgottenPasswordToken;
	}
	public void setForgottenPasswordToken(String forgottenPasswordToken) {
		this.forgottenPasswordToken = forgottenPasswordToken;
	}
	public String getArticleIdConflict() {
		return articleIdConflict;
	}
	public void setArticleIdConflict(String articleIdConflict) {
		this.articleIdConflict = articleIdConflict;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public LocalDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
}
