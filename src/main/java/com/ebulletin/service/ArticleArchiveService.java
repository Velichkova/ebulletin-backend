package com.ebulletin.service;

import com.ebulletin.domain.Article;
import com.ebulletin.domain.ArticleArchive;

public interface ArticleArchiveService {

	void save(Article article);
	
	Iterable<ArticleArchive> getAll();
}
