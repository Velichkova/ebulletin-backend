package com.ebulletin.service;

import java.io.IOException;
import java.util.UUID;
import org.apache.tika.exception.TikaException;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import com.ebulletin.domain.Article;

public interface ArticleService {

	Iterable<Article> getAll();

	Article create(Article article) throws Exception;

	void delete(String articleId);

	Article update(Article article);

	Iterable<Article> getForUser(UUID userId);

	Long getInternalId(UUID id);

	// Iterable<Article> getArticles(String userId);

	void saveFile(MultipartFile file, String articleId) throws IOException, SAXException, TikaException;
	
	void saveFeeImage(MultipartFile file, String articleId) throws IOException, SAXException, TikaException;

	byte[] getFile(String articleId);
	
	byte[] getFeeImage(String articleId);
}
