package com.ebulletin.service;

import java.io.IOException;
import java.util.UUID;

import com.ebulletin.domain.Bulletin;

public interface BulletinService {

	Iterable<Bulletin> getAll();

	Bulletin create(Bulletin bulletin) throws Exception;

	void delete(String bulletinId);

	Bulletin update(Bulletin bulletin);
	
	Long getInternalId(UUID id);

	byte[] getFile(String bulletinId);
	
	void generateFile(String bulletinId) throws IOException;
	
	Bulletin getBulletin(UUID bulletinId);

}
