package com.ebulletin.service;

import java.io.IOException;
import java.util.UUID;

import org.apache.tika.exception.TikaException;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import com.ebulletin.domain.Review;

public interface ReviewService {

	Iterable<Review> getAll();

	Review create(Review review) throws Exception;

	void delete(String articleId);

	Long getInternalId(UUID id);

	Iterable<Review> getAllForArticle(String articleId);

	void saveFile(MultipartFile file, String userId) throws IOException, SAXException, TikaException;

	Review update(Review review);

	byte[] getFiles(String articleId, int reviewNeeded);
	// List<Long> getInternalIds(Set<UUID> ids);


}
