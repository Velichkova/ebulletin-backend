package com.ebulletin.service;

import java.io.IOException;

import com.ebulletin.domain.UserRole;

public interface UserRoleService {

	Iterable<UserRole> getAll();
	
	Iterable<UserRole> create() throws IOException;
}
