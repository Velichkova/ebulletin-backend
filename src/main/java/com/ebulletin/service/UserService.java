package com.ebulletin.service;

import java.util.UUID;

import com.ebulletin.domain.User;

public interface UserService {

	Iterable<User> getAll();

	User create(User user) throws Exception;

	User authenticate(User user) throws Exception;

	void delete(String userId);

	User update(User user, boolean changePassword) throws Exception;
	
	Long getInternalId(UUID id);
	
	User getByEmail(String email);
	
	User getByForgottenPasswordToken(String token);
}
