package com.ebulletin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ebulletin.domain.Article;
import com.ebulletin.domain.ArticleArchive;
import com.ebulletin.repository.ArticleArchiveRepository;
import com.ebulletin.rest.assembler.ArticleArchiveAssembler;
import com.ebulletin.service.ArticleArchiveService;

@Component
public class ArticleArchiveServiceImpl implements ArticleArchiveService{

	@Autowired
	ArticleArchiveRepository articleArchiveRepository;
	
	@Autowired
	ArticleArchiveAssembler articleArchiveAssembler;

	@Override
	public void save(Article article) {
		ArticleArchive articleArchive = articleArchiveAssembler.fromArticle(article);
		articleArchiveRepository.save(articleArchive);
	}

	@Override
	public Iterable<ArticleArchive> getAll() {
		return articleArchiveRepository.findAll();
	}

}
