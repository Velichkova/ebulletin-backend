package com.ebulletin.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import com.ebulletin.domain.Article;
import com.ebulletin.repository.ArticleRepository;
import com.ebulletin.service.ArticleService;
import com.ebulletin.service.BulletinService;
import com.ebulletin.service.UserService;
import com.ebulletin.utils.FileUtils;

@Component
//to be reg as bean in spring context
public class ArticleServiceImpl implements ArticleService {

	private static String ARTICLE_PICTURE_DIR = "src/main/resources/articles/";

	@Autowired
	ArticleRepository articleRepository;

	@Autowired
	UserService userService;

	@Autowired
	BulletinService bulletinService;

	@Override
	public Iterable<Article> getAll() {
		return articleRepository.findAll();
	}

	@Override
	public Article create(Article article) throws Exception {
		article.setId(UUID.randomUUID());
		article.getUser().setInternalId(getUserInternalId(article.getUser().getUuid()));
		if (article.getBulletin() != null) {
			article.getBulletin().setInternalId(getBulletinInternalId(article.getBulletin().getUuid()));
		}
		articleRepository.save(article);
		return article;
	}

	@Override
	public void delete(String articleId) {
		Long internalId = articleRepository.getInternalId(UUID.fromString(articleId));
		articleRepository.deleteById(internalId);
	}

	@Override
	public Article update(Article article) {
		Long internalId = articleRepository.getInternalId(article.getUuid());
		article.setInternalId(internalId);
		article.getUser().setInternalId(getUserInternalId(article.getUser().getUuid()));
		if (article.getBulletin() != null) {
			article.getBulletin().setInternalId(getBulletinInternalId(article.getBulletin().getUuid()));
		}
		return articleRepository.save(article);
	}

	@Override
	public Iterable<Article> getForUser(UUID userId) {
		return articleRepository.findByUser(userId);
	}

	@Override
	public void saveFile(MultipartFile file, String articleId) throws IOException, SAXException, TikaException {
		String fileName = ARTICLE_PICTURE_DIR + articleId + ".pdf";
		FileUtils.save(file, fileName);

		//no chracter limit in content
		BodyContentHandler handler = new BodyContentHandler(-1);
		Metadata metadata = new Metadata();
		FileInputStream inputstream = new FileInputStream(new File(fileName));
		ParseContext pcontext = new ParseContext();

		PDFParser pdfparser = new PDFParser();
		pdfparser.parse(inputstream, handler, metadata, pcontext);

		String[] articleAtributes = handler.toString().split("\n\\s*\n");

		UUID uuid = UUID.fromString(articleId);
		Long id = articleRepository.getInternalId(uuid);
		Article article = articleRepository.findById(id).get();

		article.setArticleFile(fileName);
		article.setTitle(articleAtributes[0]);
		article.setAuthors(articleAtributes[1]);
		article.setAbstractText(articleAtributes[2]);

		articleRepository.save(article);
	}

	private Long getUserInternalId(UUID id) {
		return userService.getInternalId(id);
	}

	private Long getBulletinInternalId(UUID id) {
		return bulletinService.getInternalId(id);
	}

	@Override
	public Long getInternalId(UUID id) {
		return articleRepository.getInternalId(id);
	}

	@Override
	public byte[] getFile(String articleId) {
		String fileName = ARTICLE_PICTURE_DIR + articleId + ".pdf";
	    byte[] file = FileUtils.get(fileName);

	    return file;
	}

	@Override
	public void saveFeeImage(MultipartFile file, String articleId) throws IOException, SAXException, TikaException {
		String fileName = ARTICLE_PICTURE_DIR + articleId + "_feeImage";
	    FileUtils.save(file, fileName);

	    // save the path to the file in the DB
	    UUID uuid = UUID.fromString(articleId);
	    Long id = getInternalId(uuid);
	    Article article = articleRepository.findById(id).get();
	    article.setFeeImage(fileName);
	    articleRepository.save(article);
	}

	@Override
	public byte[] getFeeImage(String articleId) {
		String fileName = ARTICLE_PICTURE_DIR + articleId + "_feeImage";
	    byte[] file = FileUtils.get(fileName);

	    return file;
	}

}
