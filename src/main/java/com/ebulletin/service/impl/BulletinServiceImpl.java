package com.ebulletin.service.impl;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ebulletin.domain.Article;
import com.ebulletin.domain.Bulletin;
import com.ebulletin.domain.Review;
import com.ebulletin.repository.ArticleRepository;
import com.ebulletin.repository.BulletinRepository;
import com.ebulletin.repository.ReviewRepository;
import com.ebulletin.service.ArticleArchiveService;
import com.ebulletin.service.ArticleService;
import com.ebulletin.service.BulletinService;
import com.ebulletin.service.ReviewService;
import com.ebulletin.utils.FileUtils;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

@Component
public class BulletinServiceImpl implements BulletinService {

	private static String BULLETIN_FILE_DIR = "src/main/resources/bulletins/";
	private static String ARTICLE_PICTURE_DIR = "src/main/resources/articles/";
	private static String BULLETIN_COVER_DIR = "src/main/resources/bulletins/cover/";
	private static String BULLETIN_CONTENT_DIR = "src/main/resources/bulletins/content/";

	@Autowired
	BulletinRepository bulletinRepository;

	@Autowired
	ArticleRepository articleRepository;

	@Autowired
	ReviewRepository reviewRepository;

	@Autowired
	ArticleService articleService;

	@Autowired
	ReviewService reviewService;

	@Autowired
	ArticleArchiveService articleArchiveService;

	@Override
	public Iterable<Bulletin> getAll() {
		return bulletinRepository.findAll();
	}

	@Override
	public Bulletin create(Bulletin bulletin) throws Exception {
		bulletin.setId(UUID.randomUUID());
		bulletin.setVol(getVol());
		bulletin.setEditionYear(getEditionYear());
		bulletinRepository.save(bulletin);
		return bulletin;
	}

	private int getVol() {
		// works for bulletins from 2020
		// vol for 2019, +1 for every year
		int curVol = 14;
		int startYear = 2020;
		int curYear = Calendar.getInstance().get(Calendar.YEAR);

		if (curYear == startYear) {
			return 14;
		} else {
			return curVol + (curYear - startYear);
		}
	}

	private String getEditionYear() {
		int curMonth = Calendar.getInstance().get(Calendar.MONTH);
		int curYear = Calendar.getInstance().get(Calendar.YEAR);

		if (curMonth < 7) {
			return String.valueOf(1) + "/" + curYear;
		} else {
			return String.valueOf(2) + "/" + curYear;
		}
	}

	@Override
	public void delete(String bulletinId) {
		Long internalId = bulletinRepository.getInternalId(UUID.fromString(bulletinId));
		bulletinRepository.deleteById(internalId);
	}

	@Override
	public Bulletin update(Bulletin bulletin) {
		Long internalId = bulletinRepository.getInternalId(bulletin.getUuid());
		bulletin.setInternalId(internalId);
		return bulletinRepository.save(bulletin);
	}

	@Override
	public Long getInternalId(UUID id) {
		return bulletinRepository.getInternalId(id);
	}

	@Override
	public byte[] getFile(String bulletinId) {
		String fileName = BULLETIN_FILE_DIR + bulletinId + ".pdf";
		byte[] file = FileUtils.get(fileName);

		return file;
	}

	@Override
	public Bulletin getBulletin(UUID bulletinId) {

		return bulletinRepository.findById(bulletinId);
	}

	@Override
	public void generateFile(String bulletinId) throws IOException {
		Bulletin bulletin = bulletinRepository.findById(UUID.fromString(bulletinId));
		Iterable<Article> articlesForCurrentBulletin = articleRepository.getArticles(UUID.fromString(bulletinId));
		String bulletinName = BULLETIN_FILE_DIR + bulletinId + "_NOT_READY" + ".pdf";
		String readyBulletinName = BULLETIN_FILE_DIR + bulletinId + ".pdf";
		String editors = BULLETIN_COVER_DIR + "editorsNEW.pdf";
		String content = BULLETIN_CONTENT_DIR + bulletinId + "_content.pdf";

		generateFilesWithArticles(bulletinName, articlesForCurrentBulletin);
		// number of pages for every article in the bulletin
		List<Integer> pagesNumber = getPages(articlesForCurrentBulletin);
		generateContentPage(articlesForCurrentBulletin, pagesNumber, bulletinId);
		addPagesHeaderAndFooter(bulletin, bulletinName, readyBulletinName);
		addEditorsPage(bulletin, readyBulletinName);
		addCoverParameters(bulletin, bulletinName);
		mergeWithCovers(readyBulletinName, editors, content);

		// after bulletin creation
		moveArticlesForCurBulletinInArchive(articlesForCurrentBulletin);
		deleteArticlesFromTable(articlesForCurrentBulletin);
	}

	private void generateFilesWithArticles(String bulletinName, Iterable<Article> articlesForCurrentBulletin)
			throws IOException {
		List<InputStream> files = new ArrayList<>();
		for (Article article : articlesForCurrentBulletin)
			try {
				String fileName = ARTICLE_PICTURE_DIR + article.getId() + ".pdf";
				byte[] bytes = FileUtils.get(fileName);

				InputStream targetStream = new ByteArrayInputStream(bytes);
				files.add(targetStream);
			} catch (Exception e) {
				e.printStackTrace();
			}
		PDFMergerUtility ut = new PDFMergerUtility();
		for (InputStream inputStream : files) {
			ut.addSource(inputStream);
		}

		ut.setDestinationFileName(bulletinName);
		ut.mergeDocuments();
	}

	private void moveArticlesForCurBulletinInArchive(Iterable<Article> articlesForCurrentBulletin) {
		for (Article article : articlesForCurrentBulletin) {
			articleArchiveService.save(article);
		}
	}

	private void deleteArticlesFromTable(Iterable<Article> articlesForCurrentBulletin) {
		for (Article article : articlesForCurrentBulletin) {
			Iterable<Review> allReviewsForArticle = reviewService.getAllForArticle(article.getId());

			for (Review review : allReviewsForArticle) {
				reviewService.delete(review.getId());
			}

			articleService.delete(article.getId());
		}
	}

	private List<Integer> getPages(Iterable<Article> articlesForCurrentBulletin) {
		List<Integer> pagesNumber = new ArrayList<>();
		for (Article article : articlesForCurrentBulletin) {
			String fileName = ARTICLE_PICTURE_DIR + article.getId() + ".pdf";
			try {
				PdfReader reader = new PdfReader(fileName);
				int n = reader.getNumberOfPages();
				pagesNumber.add(n);
			} catch (Exception de) {
				de.printStackTrace();
			}
		}
		return pagesNumber;
	}

	private void generateContentPage(Iterable<Article> articlesForCurrentBulletin, List<Integer> pagesNumber,
			String bulletinId) {
		String filename = BULLETIN_CONTENT_DIR + bulletinId + "_content.pdf";
		String content = "Content";
		int i = 1;
		int currPage = 0;
		Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
			document.open();
			document.add(new Paragraph(content, new Font(FontFamily.HELVETICA, 16, Font.BOLD)));
			for (Article article : articlesForCurrentBulletin) {
				document.add(new Paragraph(article.getTitle(), new Font(FontFamily.HELVETICA, 12, Font.BOLD)));
				document.add(new Paragraph(article.getAuthors(), new Font(FontFamily.HELVETICA, 10, Font.ITALIC)));
				if (i == 1) {
					document.add(new Paragraph(Integer.toString(1)));
				} else if (i == 2) {
					currPage = pagesNumber.get(i - 2) + 1;
					document.add(new Paragraph(Integer.toString(currPage)));
				} else {
					document.add(new Paragraph(Integer.toString(currPage + pagesNumber.get(i - 2))));
				}
				i++;
			}
			document.close();
			writer.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void addPagesHeaderAndFooter(Bulletin bulletin, String bulletinName, String readyBulletinName) {
		try {
			PdfReader reader = new PdfReader(bulletinName);
			int n = reader.getNumberOfPages();
			int i = 0;
			PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(readyBulletinName));
			PdfContentByte over;

			while (i < n) {
				i++;
				//header
				over = stamp.getOverContent(i);
				over.beginText();
				over.setFontAndSize(BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1257, BaseFont.EMBEDDED), 12);
				over.setTextMatrix(60, 750);
				over.showText("Computer & Comunications Engineering, Vol. " + bulletin.getVol().toString() + ", No. "
						+ bulletin.getEditionYear()); 
				over.endText();
				//footer - page number
				PdfContentByte under = stamp.getOverContent(i);
				under.beginText();
				under.setFontAndSize(BaseFont.createFont(BaseFont.TIMES_BOLD, BaseFont.CP1257, BaseFont.EMBEDDED), 10);
				under.setTextMatrix(300, 40);
				under.showText("" + i);
				under.endText();
			}
			stamp.close();
		} catch (Exception de) {
			de.printStackTrace();
		}
	}

	private void addEditorsPage(Bulletin bulletin, String bulletinName) {
		try {
			PdfReader reader = new PdfReader(BULLETIN_COVER_DIR + "editors.pdf");
			PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(BULLETIN_COVER_DIR + "editorsNEW.pdf"));
			//header
			PdfContentByte over = stamp.getOverContent(1);
			over.beginText();
			over.setFontAndSize(BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1257, BaseFont.EMBEDDED), 12);
			over.setTextMatrix(60, 750);
			over.showText("Computer & Comunications Engineering, Vol. " + bulletin.getVol().toString() + ", No. "
					+ bulletin.getEditionYear());
			over.endText();
			//footer - issn parameter
			PdfContentByte under = stamp.getOverContent(1);
			under.beginText();
			under.setFontAndSize(BaseFont.createFont(BaseFont.TIMES_BOLD, BaseFont.CP1257, BaseFont.EMBEDDED), 12);
			under.setTextMatrix(70, 40);
			under.showText("ISSN " + bulletin.getIssn());
			under.endText();

			stamp.close();
		} catch (Exception de) {
			de.printStackTrace();
		}
	}

	private void addCoverParameters(Bulletin bulletin, String bulletinName) {
		try {
			PdfReader reader = new PdfReader(BULLETIN_COVER_DIR + "start.pdf");

			// applied extra content over pages
			PdfStamper stamp = new PdfStamper(reader,
					new FileOutputStream(BULLETIN_COVER_DIR + "start_param" + ".pdf"));
			// issn top parameter
			PdfContentByte over = stamp.getOverContent(1);
			over.beginText();
			over.setFontAndSize(BaseFont.createFont(BaseFont.TIMES_BOLD, BaseFont.CP1257, BaseFont.EMBEDDED), 18);
			over.setTextMatrix(450, 790);
			over.showText("ISSN " + bulletin.getIssn());
			over.endText();
			// edition year bottom parameter
			PdfContentByte under = stamp.getOverContent(1);
			under.beginText();
			under.setFontAndSize(BaseFont.createFont(BaseFont.TIMES_BOLD, BaseFont.CP1257, BaseFont.EMBEDDED), 18);
			under.setTextMatrix(450, 40);
			under.showText(bulletin.getEditionYear());
			under.endText();

			stamp.close();
		} catch (Exception de) {
			de.printStackTrace();
		}
	}

	private void mergeWithCovers(String bulletinName, String editorsName, String contentName) throws IOException {
		PDFMergerUtility ut = new PDFMergerUtility();
		String startPage = BULLETIN_COVER_DIR + "start_param" + ".pdf";
		String endPage = BULLETIN_COVER_DIR + "end" + ".pdf";

		byte[] start = FileUtils.get(startPage);
		byte[] editors = FileUtils.get(editorsName);
		byte[] content = FileUtils.get(contentName);
		byte[] bulletin = FileUtils.get(bulletinName);
		byte[] end = FileUtils.get(endPage);

		InputStream targetStreamS = new ByteArrayInputStream(start);
		InputStream targetStreamEditors = new ByteArrayInputStream(editors);
		InputStream targetStreamContent = new ByteArrayInputStream(content);
		InputStream targetStreamBulletin = new ByteArrayInputStream(bulletin);
		InputStream targetStreamE = new ByteArrayInputStream(end);

		ut.addSource(targetStreamS);
		ut.addSource(targetStreamEditors);
		ut.addSource(targetStreamContent);
		ut.addSource(targetStreamBulletin);
		ut.addSource(targetStreamE);

		ut.setDestinationFileName(bulletinName);
		ut.mergeDocuments();
	}
}
