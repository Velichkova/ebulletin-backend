package com.ebulletin.service.impl;

import java.io.IOException;
import java.util.UUID;

import org.apache.tika.exception.TikaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import com.ebulletin.domain.Review;
import com.ebulletin.repository.ReviewRepository;
import com.ebulletin.service.ArticleService;
import com.ebulletin.service.ReviewService;
import com.ebulletin.service.UserService;
import com.ebulletin.utils.FileUtils;
import com.google.common.collect.Iterables;

@Component
public class ReviewServiceImpl implements ReviewService {

	private static String REVIEW_DIR = "src/main/resources/review/";

	@Autowired
	ReviewRepository reviewRepository;

	@Autowired
	ArticleService articleService;

	@Autowired
	UserService userService;

	@Override
	public Iterable<Review> getAll() {
		return reviewRepository.findAll();
	}

	@Override
	public Review create(Review review) throws Exception {
		review.setId(UUID.randomUUID());
		review.getArticle().setInternalId(getArticleInternalId(review.getArticle().getUuid()));
		review.getUser().setInternalId(getUserInternalId(review.getUser().getUuid()));
		reviewRepository.save(review);
		return review;
	}

	@Override
	public void delete(String articleId) {
		Long internalId = reviewRepository.getInternalId(UUID.fromString(articleId));
		reviewRepository.deleteById(internalId);
	}

	@Override
	public Long getInternalId(UUID id) {
		return reviewRepository.getInternalId(id);
	}

	@Override
	public Iterable<Review> getAllForArticle(String articleId) {
		return reviewRepository.getByArticleId(UUID.fromString(articleId));
	}

	private Long getArticleInternalId(UUID id) {
		return articleService.getInternalId(id);
	}

	@Override
	public void saveFile(MultipartFile file, String reviewId) throws IOException, SAXException, TikaException {
		String fileName = REVIEW_DIR + reviewId + ".pdf";
		FileUtils.save(file, fileName);

		UUID uuid = UUID.fromString(reviewId);
		Long id = reviewRepository.getInternalId(uuid);
		Review review = reviewRepository.findById(id).get();

		review.setReviewFile(fileName);

		reviewRepository.save(review);
	}

	@Override
	public Review update(Review review) {
		Long internalId = reviewRepository.getInternalId(review.getUuid());
		review.setInternalId(internalId);
		review.getArticle().setInternalId(getArticleInternalId(review.getArticle().getUuid()));
		review.getUser().setInternalId(getUserInternalId(review.getUser().getUuid()));

		return reviewRepository.save(review);
	}

	private Long getUserInternalId(UUID id) {
		return userService.getInternalId(id);
	}

	@Override
	public byte[] getFiles(String articleId, int reviewNeeded) {
		Iterable<Review> reviews = reviewRepository.getByArticleId(UUID.fromString(articleId));
		if (Iterables.size(reviews) == 1) {
			String fileName = REVIEW_DIR + Iterables.get(reviews, 0).getId() + ".pdf";
			byte[] file = FileUtils.get(fileName);

			return file;
		} else {
			if (reviewNeeded == 1) {
				String fileName = REVIEW_DIR + Iterables.get(reviews, 0).getId() + ".pdf";
				byte[] file = FileUtils.get(fileName);

				return file;
			} else {
				String fileName = REVIEW_DIR + Iterables.get(reviews, 1).getId() + ".pdf";
				byte[] file = FileUtils.get(fileName);

				return file;
			}
		}

	}
}
