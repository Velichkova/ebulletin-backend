package com.ebulletin.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ebulletin.domain.UserRole;
import com.ebulletin.repository.UserRoleRepository;
import com.ebulletin.service.UserRoleService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Override
	public Iterable<UserRole> getAll() {
		return userRoleRepository.findAll();
	}

	@Override
	public Iterable<UserRole> create() throws IOException {
		ObjectMapper mapper = new ObjectMapper();

		String path = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator
				+ "resources" + File.separator + "databaseSetup" + File.separator + "userRoles.json";

		File file = new File(path);
		List<UserRole> userRoles = mapper.readValue(file,
				mapper.getTypeFactory().constructCollectionType(List.class, UserRole.class));
		
		return userRoleRepository.saveAll(userRoles);
	}
}
