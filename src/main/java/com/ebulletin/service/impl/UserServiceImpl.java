package com.ebulletin.service.impl;

import java.util.NoSuchElementException;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ebulletin.domain.User;
import com.ebulletin.repository.UserRepository;
import com.ebulletin.service.UserService;
import com.ebulletin.utils.CryptUtils;

@Component
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public Iterable<User> getAll() {
		return userRepository.findAll();
	}

	@Override
	public User create(User user) throws Exception {
		user.setId(UUID.randomUUID());
		user.setPassword(CryptUtils.encodeString(user.getPassword()));
		
		userRepository.save(user);

		return user;
	}

	@Override
	public User authenticate(User user) throws Exception {
		String username = user.getUsername();
		User optional = userRepository.findByUsername(username);
		User userToMatch;
		try {
			userToMatch = optional;
		} catch (NoSuchElementException e) {
			throw new SecurityException("No user with that username");
		}

		if (CryptUtils.mathches(user.getPassword(), userToMatch.getPassword())) {
			return userToMatch;
		} else {
			throw new SecurityException("Wrong password");
		}
	}

	@Override
	public void delete(String id) {
		Long internalId = userRepository.getInternalId(UUID.fromString(id));
		userRepository.deleteById(internalId);
	}

	@Override
	public User update(User user, boolean changePassword) throws Exception {
		Long internalId = userRepository.getInternalId(user.getUuid());
		user.setInternalId(internalId);
		if (user.getPassword() != null && changePassword) {
			user.setPassword(CryptUtils.encodeString(user.getPassword()));
		} else {
			User temp = userRepository.findById(user.getUuid());
			user.setPassword(temp.getPassword());
		}
		User response = userRepository.save(user);

		return response;
	}

	@Override
	public Long getInternalId(UUID id) {
		return userRepository.getInternalId(id);
	}

	@Override
	public User getByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public User getByForgottenPasswordToken(String token) {
		return userRepository.getByForgottenPasswordToken(token);
	}
}
