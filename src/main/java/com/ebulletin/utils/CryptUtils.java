package com.ebulletin.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class CryptUtils {

	public static String encodeString(String password) throws Exception {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder.encode(password);
	}

	public static boolean mathches(String raw, String encoded) throws Exception {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder.matches(raw, encoded);
	}
}
