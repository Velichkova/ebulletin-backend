package com.ebulletin.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;

public class EmailUtils {

	private static final String SENDER_USERNAME = "contact.ebulletin@gmail.com";
	private static final String SENDER_PASSWORD = "!7C*aUJyo2Ta";
	private static final String HOST = "smtp.gmail.com";
	private static final String PORT = "587";

	private static Properties getProperties() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", HOST);
		props.put("mail.smtp.port", PORT);
		props.put("mail.smtp.user", SENDER_USERNAME);
		props.put("mail.smtp.password", SENDER_PASSWORD);

		return props;
	}

	private static Session getSession() {
		Properties props = getProperties();
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(SENDER_USERNAME, SENDER_PASSWORD);
			}
		});
		return session;
	}

	public static void sendMail(String from, String to, String subject, String content) {

		Session session = getSession();

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(content, "text/plain");

			Transport.send(message);

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public static void sendSignedUp(String to) throws IOException {
		Session session = getSession();

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(SENDER_USERNAME));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject("E-bulletin credentials");

			String body = readFile("successfulSignup.html");
			body = body.replaceAll("&lt;mail&gt;", to);
			message.setContent(body, "text/html");

			Transport.send(message);
			System.out.println("E-mail sent to " + to + " with signed up notification");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void sendForgottenPassword(String email, String forgottenPasswordToken) throws IOException {
		
		Session session = getSession();

	    try {

	      Message message = new MimeMessage(session);
	      message.setFrom(new InternetAddress(SENDER_USERNAME));
	      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
	      message.setSubject("E-bulletin Password Reset");

	      String body = readFile("forgottenPassword.html");
	      body = body.replaceAll("&lt;token&gt;", forgottenPasswordToken);
	      message.setContent(body, "text/html");

	      Transport.send(message);
	      System.out.println("E-mail sent to " + email + " with password reset instructions");

	    } catch (MessagingException e) {
	      throw new RuntimeException(e);
	    }
	}
	
	private static String readFile(String path) throws IOException {
	    InputStream stream =
	        EmailUtils.class.getClassLoader().getResourceAsStream("email/" + path);
	    String output = IOUtils.toString(stream);
	    return output;
	  }
}
