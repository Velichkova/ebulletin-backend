package com.ebulletin.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.web.multipart.MultipartFile;

import com.ebulletin.exception.StorageException;
import com.ebulletin.exception.StorageFileNotFoundException;

public class FileUtils {

	public static void save(MultipartFile file, String filename) {
		if (file.isEmpty()) {
			throw new StorageException("Failed to store empty file " + filename);
		}
		File fileExist = new File(filename);
		if (fileExist.exists() && !fileExist.isDirectory()) {
			fileExist.delete();
		}
		try {
			save(file.getBytes(), filename);
		} catch (IOException e) {
			throw new StorageException("Error while storing file!", e);
		}
	}

	public static void save(byte[] file, String fileName) {
		delete(fileName);

		if (file == null) {
			throw new StorageException("Can not store empty file!");
		}

		FileOutputStream stream;
		try {
			stream = new FileOutputStream(fileName);
			IOUtils.write(file, stream);
			stream.close();
		} catch (IOException e) {
			throw new StorageException("Error while storing file: " + fileName + "!", e);
		}
	}

	public static void delete(String fileName) {
		File file = new File(fileName);
		if (file.exists() && !file.isDirectory()) {
			file.delete();
		}
	}

	public static byte[] get(String fileName) {
		FileInputStream stream;
		byte[] file = null;
		try {
			stream = new FileInputStream(fileName);
			file = IOUtils.toByteArray(stream);
			stream.close();
		} catch (IOException e) {
			throw new StorageFileNotFoundException("File: " + fileName + "not found!", e);
		}

		return file;
	}
}
